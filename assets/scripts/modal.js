var termmeta_modal = false;
var modal = false;
var file_type = false;
var modal_type = false;

jQuery(document).ready(function ($) {

	/******* Modal inagen ***********/

	//Al hacer click en el item para agregar
	jQuery(document).on('click', '.termmeta_modal .asignar, .termmeta_modal .agregar, .termmeta_modal img', function (event) {
		event.preventDefault();

		parent = jQuery(this).parents('.termmeta_modal');
		modal = jQuery(parent);
		set_to_post_id = modal.find('input[type=hidden]').val();
		var text_title = modal.data('title');
		var text_button = modal.data('button');
		var text_delete = modal.data('text-delete');
		var modal_type = modal.data('type');
		var file_type = 'image';
		var multiple = false;

		if (modal_type == 'archive') {
			file_type = 'application';
		} else if (modal_type == 'gallery') {
			file_type = 'image';
			multiple = true;
		} else {
			modal_type = 'image';
			file_type = 'image';
		}

		// If the media frame already exists, reopen it.
		if (termmeta_modal !== false) {
			termmeta_modal.dispose();
		}

		// Create the media frame.
		termmeta_modal = wp.media({
			// Set the title of the modal.
			title: text_title,
			button: {
				text: text_button
			},
			library: {
				type: file_type
			},
			multiple: multiple
		});

		//Al seleccionar un item
		termmeta_modal.on('select', function () {
			answer = false;
			selection = termmeta_modal.state().get('selection');

			selection.map(function (attachment) {

				attachment = attachment.toJSON();

				if (attachment.id) {
					jQuery(modal).addClass('seleccionada');

					//Si estamos asignando una imagen
					if (modal_type == 'image') {
						th = attachment.sizes.full.url;
						if (attachment.sizes.thumbnail !== undefined) {
							th = attachment.sizes.thumbnail.url;
						}
						jQuery(modal).find('img').attr('src', th);
						jQuery(modal).find('input[type=hidden]').val(attachment.id);
					} else if (modal_type == 'archive') { //Si estamos asignando un archivo
						jQuery(modal).find('img').attr('src', attachment.icon);
						jQuery(modal).find('.titulo').html(attachment.name);
						jQuery(modal).find('input[type=hidden]').val(attachment.id);
					} else if (modal_type == 'gallery') { //Si estamos asignando una galería
						var $product_images = jQuery(modal).find('ul.images');

						var attachment_ids = jQuery(modal).find('input[type=hidden]').val();
						attachment_ids = attachment_ids
							? attachment_ids + ',' + attachment.id
							: attachment.id;
						var attachment_image = attachment.sizes && attachment.sizes.thumbnail
							? attachment.sizes.thumbnail.url
							: attachment.url;

						jQuery(modal).find('input[type=hidden]').val(attachment_ids);

						$product_images.append('<li class="image" data-attachment_id="' + attachment.id + '"><img src="' + attachment_image + '" /><ul class="actions"><li><a class="delete" title="' + text_delete + '">' + text_delete + '</a></li></ul></li>');
					}
				}
			});

		});

		termmeta_modal.on('open activate', function () {
			// Activar el thumbnail si tenemos uno
			if (set_to_post_id) {
				var Attachment = wp.media.model.Attachment;
				var selection = termmeta_modal.state().get('selection');
				selection.add(Attachment.get(set_to_post_id));
			}
		});

		termmeta_modal.open();

	});

	//Al hacer click en eliminar
	jQuery('.termmeta_modal .eliminar').on('click', function (event) {
		var parent = jQuery(this).parents('.termmeta_modal');
		modal = jQuery(parent);

		jQuery(modal).removeClass('seleccionada');
		jQuery(modal).find('input[type=hidden]').val('');

		if (modal_type == 'image') {
			jQuery(modal).find('img').attr('src', '');
		}
	});

	// Eliminar imágenes de la galería
	jQuery('.termmeta_modal ul.images').on('click', 'a.delete', function () {
		//Parent
		var parent = jQuery(this).closest('.galeria');
		parent.find('input[type=hidden]').val('');

		//Eliminar imágen
		jQuery(this).closest('li.image').remove();

		var attachment_ids = '';

		parent.find('ul li.image').css('cursor', 'default').each(function () {
			var attachment_id = jQuery(this).attr('data-attachment_id');
			attachment_ids = attachment_ids + attachment_id + ',';
		});

		parent.find('input[type=hidden]').val(attachment_ids);

		return false;
	});

	// Image ordering
	jQuery('.termmeta_modal .galeria ul.images').sortable({
		items: 'li.image',
		cursor: 'move',
		scrollSensitivity: 40,
		forcePlaceholderSize: true,
		forceHelperSize: false,
		helper: 'clone',
		opacity: 0.65,
		placeholder: 'wc-metabox-sortable-placeholder',
		start: function (event, ui) {
			ui.item.css('background-color', '#f6f6f6');
		},
		stop: function (event, ui) {
			ui.item.removeAttr('style');
		},
		update: function () {
			//Parent
			var parent = jQuery(this).closest('.galeria');
			parent.find('input[type=hidden]').val('');

			var attachment_ids = '';

			parent.find('ul li.image').css('cursor', 'default').each(function () {
				var attachment_id = jQuery(this).attr('data-attachment_id');
				attachment_ids = attachment_ids + attachment_id + ',';
			});

			parent.find('input[type=hidden]').val(attachment_ids);
		},
	});

});
