��    	      d      �       �       �   !        $     8     M     R     X  	   a  @  k  ,   �  +   �          #     B     J     Q     a     	                                         Click to edit or update the file Click to edit or update the image Delete link to file Delete link to image File Image Set file Set image Project-Id-Version: wp_taxonomy
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-20 19:12-0500
Last-Translator: Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>
Language-Team: Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;esc_attr_e;esc_attr__
X-Poedit-Basepath: ../templates
X-Generator: Poedit 3.1.1
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Haz clic para editar o actualizar el archivo Haz clic para editar o actualizar la imagen Eliminar el enlace al archivo Eliminar el enlace a la imagen Archivo Imagen Asignar archivo Asignar imagen 