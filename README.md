# DESCRIPTION

Class to be inherited to create a WP Taxonomy.

## Mantainers

Juan Sebastián Echeverry <baxtian.echeverry@gmail.com>

## Changelog

## 0.2.14

* Include option to set base term for permalink.

## 0.2.13

* Color term with options.

## 0.2.12

* Timber 2.

## 0.2.10

* Solve some bugs.

## 0.2.9

* Use direct filename for rendering.

## 0.2.8

* Use loader/paths filter only during rendering.

## 0.2.7

* Add type file 

## 0.2.6

* Unify options list with baxtian/wp_structure 

## 0.2.5

* Bugs in list slyles 

## 0.2.4

* Bugs if term_meta is image 

## 0.2.2

* Adjusts to texts

## 0.2.1

* Recover WP_TERMMETA_V

## 0.2

* In case of using this class in multiple providers, allow Composer to set which file to use by default.

## 0.1.10

* Add meta type url.

## 0.1.9

* Allow to use PHP8.0

## 0.1.7

* Add meta type WYSIWYG.

## 0.1.6

* Include class to the script that delete image in gallery.

## 0.1.5

* Add meta type dropdown.

## 0.1.4

* Bug: Templates directory not working on some servers.

## 0.1.3

* Add meta type options

## 0.1

* First stable release
