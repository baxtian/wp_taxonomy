<?php
namespace Baxtian;

/**
 * Estructura Película
 */
class WP_Taxonomy
{
	// Datos generales
	protected $slug;
	protected $post_type;
	protected $args;

	/**
	 * Inicializa el componente
	 */
	protected function __construct()
	{
		// Incluir las opciones para determinar cómo será el slug
		add_action('admin_init', [$this, 'add_settings']);
	}

	/**
	 * Registrar la estructura
	 */
	public function init()
	{
		register_taxonomy($this->slug, $this->post_type, $this->args);
	}

	/**
	 * Incluir la opción en la página de settings de permalink para
	 * determinar cuál será el slug para 'proyecto'
	 *
	 * @return void
	 */
	public function add_settings()
	{
		$option_name = $this->slug . '_base';
		if(isset($this->args['base_setting']) && $this->args['base_setting']) {
			register_setting('permalink', $option_name);

			$label = $this->args['labels']['settings_field'];

			add_settings_field(
				$this->slug . '_base_field',
				$label,
				[$this, 'base_field_callback'],
				'permalink',
				'optional',
				[$option_name]
			);

			if(isset($_POST['permalink_structure']) && isset($_POST[$option_name])) {

				$short_domain = wp_unslash($_POST[$option_name]);
				update_option($option_name, $short_domain);

			}
		} else {
			delete_option($option_name);
		}
	}

	/**
	 * Renderizador de la entrada el setting field del
	 * slug para 'proyecto'
	 *
	 * @param array $args
	 * @return void
	 */
	public function base_field_callback($args)
	{
		$class = 'regular-text code';
		$name  = $args[0];
		$value = get_option($name);
		printf("<input type='text' class='%s' name='%s' id='%s' value='%s' />", $class, $name, $name, $value);
	}
}
