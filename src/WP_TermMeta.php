<?php

namespace Baxtian;

use Timber\Timber;
use WP_Meta_Query;

define('WP_TERMMETA_V', '0.2.13');

/**
 * Clase base para los Meta de los términos
 */
class WP_TermMeta
{
	// Datos generales
	protected $handle; // Nombre a ser usado como identificador
	protected $taxonomies      = [];
	protected $column_position = -1;
	protected $variables       = [];

	// Datos que se calcularán
	protected $column_vars   = [];
	protected $sortable_vars = [];

	/**
	 * Constructor del TermMeta
	 */
	protected function __construct()
	{
		add_action('admin_enqueue_scripts', [$this, 'admin_enqueue_scripts'], 11);
	}

	/**
	 * Generar los datos que deben ser calculados, declarar la funcionalidad para el
	 * Rest API y declarar las columnas para cada taxonomía con la que se vincule este TermMeta
	 */
	protected function init()
	{
		// Inicializar traducción
		$domain = 'wp_taxonomy';
		$locale = apply_filters('plugin_locale', determine_locale(), $domain);

		$wp_component = basename(WP_CONTENT_URL);
		$_path        = explode($wp_component, __DIR__);
		$path         = dirname(WP_CONTENT_DIR . $_path[1]) . '/languages/';
		$mofile       = $path . $domain . '-' . $locale . '.mo';

		load_textdomain($domain, $mofile);

		// Crear el arreglo con las variables que serán editadas por quickedit
		foreach ($this->variables as $item) {
			if ($item['column']) {
				$this->column_vars[$item['name']] = $item;
			}

			if (!isset($item['sortable']) || $item['sortable']) {
				$this->sortable_vars[$item['name']] = $item;
			}
		}

		// Inicializar en el rest api las variables que son quick edit\
		// ToDO: ¿Esto se debe usar solo si no está registrada la estructura?
		add_action('rest_api_init', [$this, 'api']);

		// Vincular el almacenamiento de la estructura con estas variables
		foreach ($this->taxonomies as $taxonomy) {
			// Visualizar
			add_action($taxonomy . '_add_form_fields', [$this, 'render']);
			add_action($taxonomy . '_edit_form_fields', [$this, 'render']);

			// Guardar
			add_action('edit_' . $taxonomy, [$this, 'save']);
			add_action('create_' . $taxonomy, [$this, 'save']);

			// Manejo de Columnas
			add_filter('manage_edit-' . $taxonomy . '_columns', [$this, 'columns']);
			add_filter('manage_' . $taxonomy . '_custom_column', [$this, 'columns_content'], 10, 3);

			// Determinar ordenamiento
			add_filter('manage_edit-' . $taxonomy . '_sortable_columns', [$this, 'sortable_columns']);
			add_action('pre_get_terms', [$this, 'orderby_columns'], 1);
		}
	}

	/**
	 * Generador del arreglo a ser enviado con register_rest_field
	 * @param  string $slug        Taxonomía
	 * @param  string $description Nombre del campo
	 * @return array               El arreglo requerido para ser llamado por register_rest_field
	 */
	private function api_args($slug, $description)
	{
		return [
			'get_callback' => function ($object, $field_name, $request) {
				return get_term_meta($object['id'], $field_name, true);
			},
			'update_callback' => function ($value, $object, $field_name) {
				return update_term_meta($object->ID, $field_name, wp_slash($value));
			},
			'schema' => [$slug => $description],
		];
	}

	/**
	 * Registrar el meta term en WPJson.
	 * ToDo: ¿Usar si la taxonomía ya exisitía y no se ha declarado el schema para estos term meta?
	 */
	public function api()
	{
		foreach ($this->taxonomies as $taxonomy) {
			foreach ($this->variables as $item) {
				register_rest_field(
					$taxonomy,
					$item['name'],
					$this->api_args($item['name'], $item['label'])
				);
			}
		}
	}

	/**
	 * Incluir los scripts modal en caso de ser requeridos
	 * @param  string $hook Gancho
	 */
	public function admin_enqueue_scripts($hook)
	{
		// Directorio de assets
		$wp_component = basename(WP_CONTENT_URL);
		$path         = str_replace(DIRECTORY_SEPARATOR, '/', explode($wp_component, __DIR__));
		$url          = dirname(content_url($path[1])) . '/' ;

		wp_register_style('termmeta_style', $url . 'assets/css/admin.css', [], WP_TERMMETA_V);
		wp_register_style('termmeta_modal', $url . 'assets/css/modal.css', [], WP_TERMMETA_V);
		wp_register_script('termmeta_modal', $url . 'assets/scripts/modal.js', [], WP_TERMMETA_V);

		wp_enqueue_style('termmeta_style');

		//¿Require modal?
		$require_modal = false;
		foreach ($this->variables as $key => $var) {
			if ($var['type'] == 'image') {
				$require_modal = true;

				break;
			}
		}

		if ($require_modal) {
			//Activar mecanismo para vincular medios
			wp_enqueue_media();
			wp_enqueue_script('termmeta_modal');
			wp_enqueue_style('termmeta_modal');
		}
	}

	/**
	 * Caja para ser renderizada al momento de ingresar datos de una taxonomía
	 * @param  WP_Term|boolean $term Término que se está editando en caso de ser una edición
	 */
	public function render($term)
	{
		$args = [];

		$vars = $this->variables;

		// Si hay un objeto es porque estamos editando
		if (is_object($term)) {
			foreach ($vars as $key => $var) {
				switch ($var['type']) {
						case 'image':
							$imagen_logo = get_term_meta($term->term_id, $var['name'], true);
							if ($imagen_logo) {
								$src                 = wp_get_attachment_image_src($imagen_logo, 'thumbnail');
								$vars[$key]['value'] = [
									'id'  => $imagen_logo,
									'src' => $src[0] ?? ''
								];
							}

							break;
						case 'file':
							$file_id = get_term_meta($term->term_id, $var['name'], true);
							if ($file_id) {
								$src                 = wp_get_attachment_image_src($file_id, 'thumbnail', true);
								$vars[$key]['value'] = [
									'id'  => $file_id,
									'src' => $src[0] ?? '',
									'name' => get_the_title($file_id),
								];
							}

							break;
						case 'wysiwyg':
							$vars[$key]['value']    = get_term_meta($term->term_id, $var['name'], true);
							$vars[$key]['settings'] = [
								'wpautop'       => true,
								'media_buttons' => true,
								'quicktags'     => true,
								'textarea_rows' => '15',
								'textarea_name' => $var['name'],
							];

							break;
						default:
							$vars[$key]['value'] = get_term_meta($term->term_id, $var['name'], true);

							break;
					}
			}

			// Indicar al twig que estamos editando
			$args['edit'] = true;
		}

		// Asignar variables
		$args['vars'] = $vars;
		$args['path'] = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'templates' . DIRECTORY_SEPARATOR;

		// Obtener contexto para timber
		$context = Timber::context();

		$filename = $args['path'] . 'taxonomies/termmeta.twig'; 

		// Render
		Timber::render($filename, $args);
	}

	/**
	 * Guardar los datos en sus respectivos termmeta
	 * @param  int $term_id ID del término que se esta almacenando
	 */
	public function save($term_id)
	{
		global $ag_save_flag, $post;

		// Verificar que no sea una rutina de autoguardado.
		// Si es así, el formulario no ha sido enviado y por lo tanto cancelamos
		// el proceso.
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
			return;
		}

		// Revisar permisos del usuario
		if (!current_user_can('manage_categories')) {
			return;
		}

		// Almacenar variables

		// Si estamos editando desde la vista de edición o desde el agregador
		if (isset($_POST['action']) && in_array($_POST['action'], ['editedtag', 'add-tag'])) {
			foreach ($this->variables as $key => $var) {
				switch ($var['type']) {
						case 'checkbox':
							update_term_meta($term_id, $var['name'], isset($_POST[$var['name']]));

							break;
						case 'url':
							update_term_meta($term_id, $var['name'], esc_url($_POST[$var['name']]));

							break;
						case 'options':
							$selected = [];
							if (isset($_POST[$var['name']])) {
								foreach ($_POST[$var['name']] as $key => $item) {
									$selected[] = $key;
								}
							}
							update_term_meta($term_id, $var['name'], $selected);

							break;
						default:
							update_term_meta($term_id, $var['name'], $_POST[$var['name']]);

							break;
					}
			}
		}
	}

	/**
	 * Determinar las columnas a ser visualizadas en el visor de la lista de cada taxonomía
	 * a la que fue vinculada este TermMeta
	 * @param  array $columns Lista de columnas que actualmente está asignada a la taxonomía
	 * @return array          Lista con las nuevas columnas
	 */
	public function columns($columns)
	{
		// Determinar las nuevas columnas
		$new_columns = [];
		foreach ($this->column_vars as $key => $item) {
			$new_columns[$key] = $item['label'];
		}

		// Determinar posición en que irá la columna
		$pos = $this->column_position;
		if ($this->column_position < 0) {
			$pos = count($columns) + $this->column_position;
		}

		// Mezclar las columnas
		$columns = array_merge(
			array_slice($columns, 0, $pos),
			$new_columns,
			array_slice($columns, $pos)
		);

		// Retornar columnas
		return $columns;
	}

	/**
	 * Contenido a mostrar en cada columna
	 * @param  string $content Contenido actual
	 * @param  string $column  Slug de la columna
	 * @param  int    $term_id ID del term que se está visualizando
	 */
	public function columns_content($content, $column, $term_id)
	{
		$vars = $this->column_vars;
		if (isset($vars[$column])) {
			$var = $vars[$column];
			switch ($var['type']) {
				case 'color':
					$data = get_term_meta($term_id, $column, true);
					if (!empty($data)) {
						printf('<div class="termmeta-color-block" data-%s="%2$s" style="background-color: %2$s;"></div>', $column, $data);
					}

					break;
				case 'image':
					$imagen_logo = get_term_meta($term_id, $column, true);
					if ($imagen_logo) {
						$src = wp_get_attachment_image_src($imagen_logo, 'thumbnail');
						if (!empty($src)) {
							printf('<img class="termmeta-image-block" data-%s="%s" src="%s">', $column, $imagen_logo, $src[0]);
						}
					}

					break;
				case 'file':
					$file_id = get_term_meta($term_id, $column, true);
					if ($file_id) {
						$title = get_the_title($file_id);
						$src = wp_get_attachment_url($file_id);
						printf('<a href="%s" target="_BLANK" class="termmeta-file-block" data-%s="%s">%s</label>', $src, $column, $file_id, $title);
					}

					break;
				case 'checkbox':
					$content = (get_term_meta($term_id, $column, true)) ? "<i class='dashicons dashicons-yes' data-" . $column . "='1'></i>" : "<i class='dashicons dashicons-no-alt' data-" . $column . "='0'></i>";
					echo $content;

					break;
				case 'dropdown':
				case 'options':
						$selected = get_term_meta($term_id, $column, true);
						if(!is_array($selected)) $selected = (array)$selected;
						$options  = $var['options'];
						echo '<ul class="wp_termmeta_list">';
						foreach ($options as $key => $value) {
							// Si es un arreglo descriptivo, extrae rlos elementos
							if(is_array($value)) {
								$key = $value['value'];
								$value = $value['label'];
							}

							if (in_array($key, $selected)) {
								printf('<li>%s</li>', $value);
							}
						}
						echo '</ul>';

					break;
				default:
					$data = get_term_meta($term_id, $column, true);
					if (!empty($data)) {
						printf('<span data-%s="%2$s">%2$s</span>', $column, $data);
					}

					break;
			}
		}
	}

	/**
	 * Determinar los campos por los que se puede ordenar
	 * @param  array $post_columns Columnas por las que se puede ordenar actualmente
	 * @return array               Arreglo de columnas incluyendo las que se defineron como ordenables en este TermMeta
	 */
	public function sortable_columns($post_columns)
	{
		// Determinar las columnas ordenables
		$new_columns = [];
		foreach ($this->sortable_vars as $key => $item) {
			$new_columns[$key] = $key;
		}

		// Agregar los campos propios de este término y que estarán
		// disponibles para el ordenamiento de la lista
		$post_columns = array_merge(
			$post_columns,
			$new_columns
		);

		// Retornar nuevas columnas ordenables
		return $post_columns;
	}

	/**
	 * Ordenar según los campos propios de este TermMeta
	 * @param  WP_Query $query Instancia de consulta
	 * @return WP_Quert        Instancia con el ordenamiento
	 */
	public function orderby_columns($query)
	{
		global $pagenow;

		// Determinar cuál va a ser el termmeta por el cuál se está ordenando
		$orderby_items = [];
		foreach ($this->sortable_vars as $key => $item) {
			$orderby_items[] = $item['name'];
		}

		$orderby = (isset($_GET['orderby'])) ? $_GET['orderby'] : false;

		// Este ordenamiento solo se hace si estamos viendo la página de edit-tags y tenemos un orderby
		if (is_admin() && $pagenow == 'edit-tags.php' && $orderby) {
			// ¿Es un ordenamiento que tenemos en la lista de ordenamientos disponibles?
			if (in_array($orderby, $orderby_items)) {
				// Asignar orderby a la clausula del meta_query
				$query->query_vars['orderby'] = $orderby;
				$args                         = [
					'relation'     => 'OR',
					'order_clause' => [
						'key' => $orderby,
					],
					[
						'key'     => $orderby,
						'compare' => 'NOT EXISTS',
					],
				];

				//Asignar el MetaQuery a la consulta actual
				$query->meta_query = new WP_Meta_Query($args);
			}
		}

		// Retornar consulta
		return $query;
	}
}
